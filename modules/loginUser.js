const generateToken = require('../utils/generateToken');
const Users = require('../models/Users');
const RefreshTokens = require('../models/RefreshTokens');
const config = require('../config.json');

module.exports = (user) => {
  return new Promise(async (resolve, reject) => {
    try {
      const { username, id, password } = user.toJSON();
      const accressTokenPromise = generateToken({ username, id }, process.env.ACCESS_TOKEN_SECRET, { expiresIn: config.accessTimeout });
      const refreshTokenPromise = generateToken({ username, id }, process.env.REFRESH_TOKEN_SECRET + password);

      const [accessToken, refreshToken] = await Promise.all([accressTokenPromise, refreshTokenPromise]);

      const tokenData = await RefreshTokens.findOne({
        where: {
          user_id: id,
        },
      });

      // const tokenQuery = await db.query('select * from refresh_tokens where user_id = $1', [id]);

      if (tokenData) {
        tokenData.token = refreshToken;
        // db.query('update refresh_tokens set token = $1 where user_id = $2', [refreshToken, id]);
        tokenData.save();
      } else {
        const newToken = RefreshTokens.build({
          user_id: id,
          token: refreshToken,
        });
        // db.query('insert into refresh_tokens (user_id,token) values ($1,$2)', [id, refreshToken]);
        newToken.save();
      }

      resolve({ accessToken, refreshToken });
    } catch (error) {
      console.log(error);
    }
  });
};

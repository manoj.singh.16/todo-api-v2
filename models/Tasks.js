const Sequelize = require('sequelize');
const db = require('../db');
const SubTasks = require('./SubTasks');
const Users = require('./Users');

const Tasks = db.define(
  'tasks',
  {
    text: {
      type: Sequelize.STRING,
    },
    is_completed: {
      type: Sequelize.BOOLEAN,
    },
    user_id: {
      type: Sequelize.INTEGER,
    },
  },
  {
    timestamps: false,
  }
);

Tasks.hasMany(SubTasks, { foreignKey: 'task_id' });
SubTasks.belongsTo(Tasks, { foreignKey: 'task_id' });

module.exports = Tasks;

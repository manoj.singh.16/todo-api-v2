const Sequelize = require('sequelize');
const db = require('../db');
const Users = require('./Users');

const RefreshTokens = db.define(
  'refresh_tokens',
  {
    user_id: {
      type: Sequelize.INTEGER,
    },
    token: {
      type: Sequelize.STRING,
    },
  },
  { timestamps: false }
);

module.exports = RefreshTokens;

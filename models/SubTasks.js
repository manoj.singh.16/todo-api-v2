const Sequelize = require('sequelize');
const db = require('../db');

const SubTasks = db.define(
  'sub_tasks',
  {
    text: {
      type: Sequelize.STRING,
    },
    is_completed: {
      type: Sequelize.BOOLEAN,
    },
    task_id: {
      type: Sequelize.INTEGER,
    },
  },
  {
    timestamps: false,
  }
);

module.exports = SubTasks;

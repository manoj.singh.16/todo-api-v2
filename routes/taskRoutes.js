const express = require('express');
const { getAllTasks, postTask, getSingleTask, deleteTask, updateTask } = require('../controllers/tasksController');
const verifyAuth = require('../middlewares/verifyAuth');

const router = express.Router();

// Get all tasks
router.get('/', getAllTasks);

// Get single tasks
router.get('/:id', getSingleTask);

// Create Task
router.post('/', verifyAuth, postTask);

// Update Task
router.patch('/:id', verifyAuth, updateTask);

// Delete task
router.delete('/:id', verifyAuth, deleteTask);

module.exports = router;

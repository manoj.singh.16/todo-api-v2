const forgotPassword = require('../controllers/forgotPassword');
const loginController = require('../controllers/loginController');
const logoutController = require('../controllers/logoutController');
const registerController = require('../controllers/registerController');
const revalidateController = require('../controllers/revalidateController');
const verifyController = require('../controllers/verifyController');
const changePassword = require('../controllers/changePassword');
const deactivateContoller = require('../controllers/deactivateContoller');
const verifyAuth = require('../middlewares/verifyAuth');

const router = require('express').Router();

// User Registration
router.post('/register', registerController);

// User Login
router.post('/login', loginController);

// Revalidate Access Token
router.get('/revalidate-access', revalidateController);

// Verify user login
router.get('/verify', verifyController);

// User logout
router.post('/logout', logoutController);

// forgot password - to get reset token
router.post('/forgot-password', forgotPassword);

// change password
router.patch('/change-password', changePassword);

// change password
router.delete('/deactivate', verifyAuth, deactivateContoller);

module.exports = router;

const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const config = require('../config.json');
const changePasswordValidator = require('../validations/changePasswordValidator');
const Users = require('../models/Users');

module.exports = async (req, res) => {
  try {
    const tokenHeader = req.headers['x-forgot-token'];
    const forgotToken = tokenHeader.split(' ')[1];
    const { id, username } = jwt.decode(forgotToken);

    const user = await Users.findOne({
      where: {
        id,
      },
    });

    const decoded = jwt.verify(forgotToken, process.env.PASSWORD_TOKEN_SECRET + user.password);

    const { error, isValid } = changePasswordValidator(req.body);

    if (!isValid) {
      return res.status(400).json(error);
    }

    const { password: newPassword } = req.body;

    const newHash = await bcrypt.hash(newPassword, config.saltRounds);

    user.password = newHash;

    await user.save();

    res.json({ msg: 'password updated' });
  } catch (error) {
    console.log(error);
    res.status(400).json({ msg: 'Invalid Request' });
  }
};

const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const registerValidator = require('../validations/registerValidator');
const Users = require('../models/Users');
const { saltRounds } = require('../config.json');
const loginUser = require('../modules/loginUser');
const { Op } = require('sequelize');

module.exports = async (req, res) => {
  const { error, isValid } = registerValidator(req.body);

  if (!isValid) {
    return res.status(400).json(error);
  }

  try {
    const { username, password, email } = req.body;

    const data = await Users.findOne({
      where: {
        username,
        [Op.or]: [
          {
            email,
          },
        ],
      },
    });

    // const data = await db.query('select username,email from users where username = $1 or email = $2', [username, email]);

    if (data && username === data.username) {
      return res.status(400).json({ msg: `username already exist` });
    } else if (data && email === data.email) {
      return res.status(400).json({ msg: `email already exist` });
    }

    const hash = await bcrypt.hash(password, saltRounds);

    const newUser = Users.build({
      username,
      email,
      password: hash,
    });

    const user = await newUser.save();
    const { accessToken, refreshToken } = await loginUser(user);

    res.status(201).json({ accessToken, refreshToken });
  } catch (error) {
    console.log(error);
  }
};

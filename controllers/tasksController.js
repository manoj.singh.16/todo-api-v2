const Tasks = require('../models/Tasks');
const isEmpty = require('../utils/isEmpty');

const getAllTasks = async (req, res) => {
  try {
    const data = await Tasks.findAll();

    console.log(data);

    res.json({ data });
  } catch (error) {
    console.log(error);
  }
};

const getSingleTask = async (req, res) => {
  try {
    const taskId = req.params.id;
    const data = await Tasks.findOne({
      where: {
        id: taskId,
      },
    });

    if (data) {
      res.json({ data });
    } else {
      res.json({ data: {} });
    }
  } catch (error) {
    console.log(error);
  }
};

const postTask = async (req, res) => {
  try {
    const { user } = req;
    const { text } = req.body;

    const newTask = await Tasks.build({
      text,
      user_id: user.id,
    });

    const task = await newTask.save();
    console.log(task);

    res.status(201).json({ data: task });
  } catch (error) {
    console.log(error);
  }
};

const deleteTask = async (req, res) => {
  try {
    const taskId = req.params.id;

    const { user } = req;
    const task = await Tasks.findOne({
      where: {
        id: taskId,
      },
    });

    if (task.user_id !== user.id) {
      return res.status(401).json({ error: 'Not authorized' });
    }

    const deletedTask = await task.destroy();

    if (deletedTask) {
      res.json({
        data: {
          msg: 'deleted successfully',
        },
      });
    } else {
      res.status(400).json({
        data: {
          msg: 'no task with the given id',
        },
      });
    }
  } catch (error) {
    res.status(400).json({ error: 'Invalid request' });
  }
};

const updateTask = async (req, res) => {
  try {
    const taskId = req.params.id;
    const { text, is_completed } = req.body;

    const { user } = req;
    const task = await Tasks.findOne({
      where: {
        id: taskId,
      },
    });

    if (task.user_id !== user.id) {
      return res.status(401).json({ error: 'Not authorized' });
    }

    if (text && is_completed) {
      task.text = text;
      task.is_completed = is_completed;
      await task.save();
      return res.json({ data: task });
    } else if (text) {
      task.text = text;
      await task.save();

      return res.json({ data: task });
    } else if (is_completed) {
      task.is_completed = is_completed;
      await task.save();
      res.json({ data: task });
    } else {
      res.status(400).json({ error: 'Invalid request' });
    }
  } catch (error) {
    res.status(400).json({ error: 'Invalid request' });
  }
};

module.exports = {
  getAllTasks,
  postTask,
  getSingleTask,
  deleteTask,
  updateTask,
};

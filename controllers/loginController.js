const loginValidator = require('../validations/loginValidator');
const bcrypt = require('bcrypt');
const Users = require('../models/Users');
const loginUser = require('../modules/loginUser');

module.exports = async (req, res) => {
  const { error, isValid } = loginValidator(req.body);

  if (!isValid) {
    return res.status(400).json(error);
  }

  try {
    const { username, password } = req.body;
    const user = await Users.findOne({
      where: {
        username,
      },
    });

    if (!user) {
      return res.status(401).json({ msg: 'No user found' });
    }

    const result = await bcrypt.compare(password, user.password);

    if (!result) {
      return res.status(401).json({ msg: 'Password incorrect' });
    }

    const { accessToken, refreshToken } = await loginUser(user);

    res.json({ accessToken, refreshToken });
  } catch (error) {
    console.log(error);
  }
};

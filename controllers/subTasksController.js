const SubTasks = require('../models/SubTasks');
const Tasks = require('../models/Tasks');

const getAllSubTasks = async (req, res) => {
  try {
    const subTasks = await SubTasks.findAll();

    res.json({ data: subTasks });
  } catch (error) {
    console.log(error);
  }
};

const getSingleSubTask = async (req, res) => {
  try {
    const subTaskId = req.params.id;

    const subTask = await SubTasks.findOne({
      where: {
        id: subTaskId,
      },
    });

    if (subTask) {
      res.json({ data: subTask });
    } else {
      res.json({ data: {} });
    }
  } catch (error) {
    console.log(error);
  }
};

const postSubTask = async (req, res) => {
  try {
    const { text, task_id } = req.body;

    const newSubTask = SubTasks.build({
      text,
      task_id,
    });

    await newSubTask.save();

    res.status(201).json({ data: newSubTask });
  } catch (error) {
    res.status(400).json({ error: 'Invalid Request' });
  }
};

const deleteSubTask = async (req, res) => {
  try {
    const subTaskId = req.params.id;

    const { user } = req;
    const subTask = await SubTasks.findOne({
      where: {
        id: subTaskId,
      },
      include: [Tasks],
    });

    if (!subTask) {
      return res.status(404).json({ error: 'No sub task with the given id' });
    }

    if (subTask.task.user_id !== user.id) {
      return res.status(401).json({ error: 'Not authorized' });
    }

    await subTask.destroy();

    res.json({
      data: {
        msg: 'deleted successfully',
      },
    });
  } catch (error) {
    res.status(400).json({ error: 'Invalid request' });
  }
};

const updateSubTask = async (req, res) => {
  try {
    const subTaskId = req.params.id;
    const { text, is_completed } = req.body;

    const { user } = req;
    const subTask = await SubTasks.findOne({
      where: {
        id: subTaskId,
      },
      include: [Tasks],
    });

    if (subTask.task.user_id !== user.id) {
      return res.status(401).json({ error: 'Not authorized' });
    }

    if (text && is_completed) {
      subTask.text = text;
      subTask.is_completed = is_completed;
      await subTask.save();
      res.json({ data: subTask });
    } else if (text) {
      subTask.text = text;
      await subTask.save();
      res.json({ data: subTask });
    } else if (is_completed) {
      subTask.is_completed = is_completed;
      subTask.save();
      res.json({ data: subTask });
    } else {
      res.status(400).json({ error: 'Invalid request' });
    }
  } catch (error) {
    console.log(error);
    res.status(400).json({ error: 'Invalid request' });
  }
};

module.exports = {
  getAllSubTasks,
  postSubTask,
  getSingleSubTask,
  updateSubTask,
  deleteSubTask,
};

const jwt = require('jsonwebtoken');
const Tasks = require('../models/Tasks');
const Users = require('../models/Users');

module.exports = async (req, res) => {
  try {
    const accessTokenHeader = req.headers['authorization'];
    const accessToken = accessTokenHeader.split(' ')[1];
    const decoded = jwt.verify(accessToken, process.env.ACCESS_TOKEN_SECRET);

    res.json({ username: decoded.username, id: decoded.id });
  } catch (error) {
    console.log(error);
    res.status(400).json({ msg: 'Invalid token' });
  }
};

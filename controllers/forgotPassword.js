const Users = require('../models/Users');
const generateToken = require('../utils/generateToken');

module.exports = async (req, res) => {
  try {
    const { email } = req.body;

    const user = await Users.findOne({
      where: {
        email,
      },
    });

    console.log(user.username);

    const token = await generateToken({ username: user.username, id: user.id, email: user.email }, process.env.PASSWORD_TOKEN_SECRET + user.password);

    res.json({ token });
  } catch (error) {
    console.log(error);
    res.status(404).json({ msg: 'No user found with that email' });
  }
};

const jwt = require('jsonwebtoken');
const generateToken = require('../utils/generateToken');
const db = require('../db');
const RefreshTokens = require('../models/RefreshTokens');
const Users = require('../models/Users');

module.exports = async (req, res) => {
  try {
    const refreshHeader = req.headers['x-refresh-token'];
    const refreshToken = refreshHeader.split(' ')[1];
    const { username, id } = jwt.decode(refreshToken);

    const refreshData = await RefreshTokens.findOne({
      where: {
        user_id: id,
      },
    });

    if (refreshData.token === refreshToken) {
      const user = await Users.findOne({
        where: {
          id,
        },
      });

      jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET + user.password, async (error, decoded) => {
        if (error) {
          console.log(error);
        } else {
          const accessToken = await generateToken(decoded, process.env.ACCESS_TOKEN_SECRET, { expiresIn: 3600 });
          res.json({ accessToken });
        }
      });
    } else {
      throw new Error('Invalid refresh token');
    }
  } catch (error) {
    res.status(400).json({ msg: 'Invalid token' });
  }
};

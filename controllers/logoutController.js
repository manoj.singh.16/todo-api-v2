const jwt = require('jsonwebtoken');
const db = require('../db');
const RefreshTokens = require('../models/RefreshTokens');

module.exports = async (req, res) => {
  try {
    const refreshHeader = req.headers['x-refresh-token'];
    const refreshToken = refreshHeader.split(' ')[1];

    const deletedToken = await RefreshTokens.destroy({
      where: {
        token: refreshToken,
      },
    });

    if (deletedToken) {
      res.json({ msg: 'Logout Success' });
    } else {
      throw new Error('No Refresh Token in DB');
    }
  } catch (error) {
    console.log(error);
    res.status(401).json({ msg: 'Invalid Token' });
  }
};

const db = require('../db');
const Users = require('../models/Users');

module.exports = async (req, res) => {
  try {
    const { user } = req;

    const deletedUser = await Users.destroy({
      where: {
        id: user.id,
      },
    });
    if (deletedUser) {
      res.json({ msg: 'Successfully deactivated' });
    } else {
      res.status(400).json({ msg: 'User doesnt exist' });
    }
  } catch (error) {
    console.log(error);
  }
};

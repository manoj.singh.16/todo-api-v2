## API Endpoints

### Auth routes

POST - `/auth/login` - user login using username and password\
POST - `/auth/register` - user register\
GET - `/auth/revalidate-access` - revalidate access token using refresh token\
GET - `/auth/verify` - verify loggedin user using access token\
GET - `/auth/logout` - logout user\
POST - `/auth/forgot-password` - generate token for password reset\
PATCH - `/auth/change-password` - change password using the token\
DELETE - `/auth/deactivate` - delete user and data from database\

### Tasks routes

GET - `/tasks` - get all tasks\
GET - `/tasks/:id` - get single task\
POST - `/tasks` - create a task\
DELETE - `/tasks/:id` - delete a task\
PATCH - `/tasks/:id` - update a task\

### Sub Tasks routes

GET - `/sub-tasks` - get all sub tasks\
GET - `/sub-tasks/:id` - get single sub task\
POST - `/sub-tasks` - create a sub task\
DELETE - `/sub-tasks/:id` - delete a sub task\
PATCH - `/sub-tasks/:id` - update a sub task\
